package ar.edu.unlp.info.bd2.services;

import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.repositories.BithubRepository;
import org.jetbrains.annotations.Contract;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.Name;
import java.util.*;

public class BithubServiceImpl implements BithubService{

    private BithubRepository repository;

    public BithubServiceImpl(BithubRepository repository){
        this.repository = repository;
    }

    @Transactional
    @Override
    public User createUser(String email, String name)
    {
        User u = new User();
        u.setName(name);
        u.setEmail(email);
        return this.repository.createUser(u);
    }

    @Transactional
    @Override
    public Optional<User> getUserByEmail(String email)
    {
        return Optional.empty();
    }


    @Transactional
    @Override
    public Branch createBranch(String name) {
        Branch b = new Branch(name);
        return this.repository.createBranch(b);
    }

    @Transactional
    @Override
    public Commit createCommit(String description, String hash, User author, List<File> files, Branch branch) {
        Commit c = new Commit(description, hash, author, files, branch);
        return this.repository.createCommit(c);
    }

    @Transactional
    @Override
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {
        Optional <Commit> c = this.repository.getCommitByHash(commitHash);
        if (c.isPresent()){
            Tag tag = new Tag(c.get(),name);
            return  this.repository.createTagForCommit(tag);
        }
        else {
            throw new BithubException("BithubException");
        }

    }

    @Transactional
    @Override
    public Optional<Commit> getCommitByHash(String commitHash)
    {
        return this.repository.getCommitByHash(commitHash);
    }

    @Transactional
    @Override
    public File createFile(String content,String name)
    {
        File f = new File(content,name);
        return this.repository.createFile(f);
    }

    @Transactional
    @Override
    public Optional<Tag> getTagByName(String tagName)
    {
        return this.repository.getTagByName(tagName);
    }

    @Transactional
    @Override
    public Review createReview(Branch branch, User user)
    {
        Review r = new Review(branch, user);
        return this.repository.createReview(r);
    }

    @Transactional
    @Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException{
        if (review.getBranch().equals(file.getCommit().getBranch()))
        {
            FileReview f = new FileReview(review,file,lineNumber,comment);
            return this.repository.addFileReview(f);
        }
        else{
            throw new BithubException("BithubException");
        }

    }

    @Transactional
    @Override
    public Optional<Review> getReviewById(long id){ return this.repository.getReviewById(id);}

    @Transactional
    @Override
    public List<Commit> getAllCommitsForUser(long userId)
    {
        return this.repository.getAllCommitsForUser(userId);
    }

    @Transactional
    @Override
    public Map<Long, Long> getCommitCountByUser()
    {
        return this.repository.getCommitCountByUser();
    }

    @Transactional
    @Override
    public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException
    {
        Optional<Branch> branch = this.getBranchByName(branchName);
        if (branch.isPresent()){
            return this.repository.getUsersThatCommittedInBranch(branchName);
        }
        else{
            throw new BithubException("BithubException");
        }
    }

    @Transactional
    @Override
    public Optional<Branch> getBranchByName(String branchName)
    {
        return this.repository.getBranchByName(branchName);
    }
}
