package ar.edu.unlp.info.bd2.model;

import ar.edu.unlp.info.bd2.services.BithubException;

import javax.persistence.*;


@Table(name = "FileReview")
@Entity
public class FileReview {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "fileReview_id")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "review_id")
    private Review review;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "file_id")
    private File reviewedFile;

    @Column(name = "lineNumber", nullable = false, length = 50)
    private Integer lineNumber;

    @Column(name = "comment", nullable = false, length = 50)
    private String comment;

    public  FileReview(){

    }
    //NOSE SI ESTA BIEN
    public FileReview(Review review, File file, int lineNumber, String comment){
        this.review= review;
        this.review.getReviews().add(this);
        this.reviewedFile=file;
        this.reviewedFile.getFileReviews().add(this);
        this.lineNumber=lineNumber;
        this.comment=comment;
    }


    public Integer getId() {
        return this.id;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Review getReview() {
        return review;
    }

    public void setReviewedFile(File file) {
        this.reviewedFile = file;
    }

    public File getReviewedFile() {
        return reviewedFile;
    }

    //como se hace
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        return null;
    }


}
