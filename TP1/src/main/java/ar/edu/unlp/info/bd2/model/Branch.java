package ar.edu.unlp.info.bd2.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Table(name="Branch")
@Entity
public class Branch {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "branch_id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="branch")
    private List<Commit> commits= new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy="branch")
    private List<Review> reviews = new ArrayList<>();

    public Branch(){}

    public Branch(String name){
        this.name= name;

    }


    public Long getId() {
        return id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Commit> getCommits(){
        return commits;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }


}
