package ar.edu.unlp.info.bd2.repositories;

import ar.edu.unlp.info.bd2.model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;


public class BithubRepository {

    @Autowired
    private SessionFactory sessionFactory;

    public User createUser(User u) {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(u);
        return u;
    }

    public User getUserByEmail(String email){
        Session session = this.sessionFactory.getCurrentSession();
        String hql ="from User as u where u.email = :email";
        Query q = session.createQuery(hql).setParameter("email", email);
        User user = (User) q.getSingleResult();

        return user;
    }

    public  Branch createBranch(Branch b){

        Session session = this.sessionFactory.getCurrentSession();
        session.save(b);
        return b;
    }

    public Commit createCommit(Commit c){
        Session session = this.sessionFactory.getCurrentSession();
        session.save(c);
        return c;
    }

    public File createFile(File f){
        Session session = this.sessionFactory.getCurrentSession();
        session.save(f);
        return f;
    }

    public Tag createTagForCommit(Tag t){
        Session session = this.sessionFactory.getCurrentSession();
        session.save(t);
        return  t;
    }

    public Optional<Commit> getCommitByHash(String commitHash) {
        String hql = "FROM Commit as c WHERE c.hash = :hash";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("hash", commitHash);
        Optional<Commit> commit = q.uniqueResultOptional();
        return commit;
    }

    public Optional<Branch> getBranchByName(String branchName) {
        String hql = "FROM Branch as b WHERE b.name = :name";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("name", branchName);
        Optional<Branch> branch = q.uniqueResultOptional();
        return branch;
    }

    public Optional <Tag> getTagByName(String name){
        String hql = "FROM Tag as t WHERE t.name = :name";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("name", name);
        Optional<Tag> tag = q.uniqueResultOptional();
        return tag;
    }

    public List<Commit> getAllCommitsForUser(Long id)
    {
        String hql = "FROM User as u WHERE u.id = :id";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("id", id);
        User u = (User) q.uniqueResult();
        return u.getCommits();
    }

    public Map<Long, Long> getCommitCountByUser()
    {
        String hql = "FROM User";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql);
        ArrayList<User> users = (ArrayList<User>) q.getResultList();
        Map <Long , Long> map = new HashMap<Long,Long>();
        users.forEach(u->map.put (u.getId(), Long.valueOf(u.getCommits().size())));
        return map;
    }

    public List<User> getUsersThatCommittedInBranch(String branchName){
        String hql = "FROM Branch as b WHERE b.name = :name";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("name", branchName);
        Branch b = (Branch) q.uniqueResult();
        Set<User> users = new HashSet<>();
        b.getCommits().forEach(c-> users.add(c.getAuthor()));
        ArrayList<User> list = new ArrayList(users);
        return list;
    }

    public Review createReview(Review r)
    {
        Session session = this.sessionFactory.getCurrentSession();
        session.save(r);
        return r;
    }

    public FileReview addFileReview(FileReview f){
        Session session = this.sessionFactory.getCurrentSession();
        session.save(f);
        return f;
    }

    public Optional<Review> getReviewById(long id){
        String hql = "FROM Review as r WHERE r.id = :id";
        Session session = this.sessionFactory.getCurrentSession();
        Query q = session.createQuery(hql).setParameter("id", id);
        Optional<Review> review = q.uniqueResultOptional();
        return review;
    }




}
