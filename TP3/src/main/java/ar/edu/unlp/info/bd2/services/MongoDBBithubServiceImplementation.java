package ar.edu.unlp.info.bd2.services;

import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.repositories.MongoDBBithubRepository;

import java.util.*;

public class MongoDBBithubServiceImplementation implements BithubService {

    MongoDBBithubRepository repositoy;

    public MongoDBBithubServiceImplementation(MongoDBBithubRepository repository) {
        this.repositoy = repository;
    }


    @Override
    public User createUser(String email, String name) {
        User u = new User(email,name);
        return this.repositoy.createUser(u);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public Branch createBranch(String name) {
        Branch b = new Branch(name);
        return this.repositoy.createBranch(b);
    }

    @Override
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {
        Optional <Commit> c = this.repositoy.getCommitByHash(commitHash);
        if (c.isPresent()){
            Tag tag = new Tag(c.get(),name);
            return  this.repositoy.createTagForCommit(tag);
        }
        else {
            throw new BithubException("BithubException");
        }
    }

    @Override
    public Optional<Commit> getCommitByHash(String commitHash) {
        return this.repositoy.getCommitByHash(commitHash);
    }

    @Override
    public File createFile(String content, String name) {
        File f = new File(content,name);
        return this.repositoy.createFile(f);
    }

    @Override
    public Optional<Tag> getTagByName(String tagName) {
        return this.repositoy.getTagByName(tagName);
    }

    @Override
    public Review createReview(Branch branch, User author) {
        Review review = new Review(branch, author);

        repositoy.createReview(review);

        repositoy.updateBranch(branch);
        repositoy.updateAuthor(author);

        repositoy.createAsocciation(review.getObjectId(),branch.getObjectId(),"ReviewBranch");
        repositoy.createAsocciation(review.getObjectId(),author.getObjectId(),"ReviewAuthor");

        return review;
    }

    @Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        Review r = this.repositoy.getFindReview(review);
        if (r.getBranch().getObjectId().equals(file.getCommit().getBranch().getObjectId()))
        {
            FileReview fileReview = new FileReview(review,file,lineNumber,comment);

            this.repositoy.addFileReview(fileReview);
            this.repositoy.updateFile(file);
            this.repositoy.updateReview(review);

            this.repositoy.createAsocciation(fileReview.getObjectId(),file.getObjectId(),"FileReviewFile");
            this.repositoy.createAsocciation(fileReview.getObjectId(),review.getObjectId(),"FileReviewReview");

            return fileReview;
        }
        else{
            throw new BithubException("BithubException");
        }

    }

    @Override
    public Optional<Review> getReviewById(Object id) {
        return this.repositoy.getReviewById(id);
    }

    @Override
    public List<Commit> getAllCommitsForUser(Object userId) {
        return this.repositoy.getAllCommitsForUser(userId);
    }

    @Override
    public Map getCommitCountByUser() {
        return this.repositoy.getCommitCountByUser();
    }

    @Override

        public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException {
        Optional<Branch> branch = this.getBranchByName(branchName);
        if (branch.isPresent()) {
            Set<User> users = new HashSet<>();
            ArrayList<Commit> commits = this.repositoy.getCommitByBranch(branch.get());
            commits.forEach(commit -> users.add(commit.getAuthor()));
            ArrayList<User> listOfUsers = new ArrayList(users);
            return listOfUsers;
        }
        else{
            throw new BithubException("BithubException");
        }
    }


    @Override
    public Optional<Branch> getBranchByName(String branchName) {
        return this.repositoy.getBranchByName(branchName);
    }

    @Override
    public Commit createCommit(String description, String hash, User author, List list, Branch branch) {
        Commit commit = new Commit(description, hash, author, list, branch);

        repositoy.createCommit(commit);
        repositoy.updateBranch(branch);
        repositoy.updateAuthor(author);

        repositoy.createAsocciation(commit.getObjectId(),branch.getObjectId(),"CommitBranch");
        repositoy.createAsocciation(commit.getObjectId(),author.getObjectId(),"CommitAuthor");
        repositoy.createAssociationFile(list,commit.getObjectId(),"FileAuthor");

        return commit;
    }
}
