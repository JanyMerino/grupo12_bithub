package ar.edu.unlp.info.bd2.model;

import org.bson.codecs.pojo.annotations.BsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name="File")
@Entity
public class File extends PersistentObject{

    @Column(name = "name", nullable = false, length = 50)
    private String name;

    @Column(name = "content", nullable = false, length = 100)
    private String content;

    @OneToMany(mappedBy = "reviewedFile")
    private List<FileReview> fileReviews = new ArrayList<>();

    @BsonIgnore
    @ManyToOne()
    @JoinColumn(name = "commit_id")
    private Commit commit;


    public File(){}

    public File(String content, String name){
        this.name = name;
        this.content = content;

    }

    public String getFilename() {
        return name;
    }

    public void setFilename(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<FileReview> getFileReviews() {
        return fileReviews;
    }

    public void setFileReviews(List<FileReview> fileReviews) {
        this.fileReviews = fileReviews;
    }

    public Commit getCommit() {
        return this.commit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCommit(Commit commit) {
        this.commit= commit;
    }
}
