package ar.edu.unlp.info.bd2.services;

import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.repositories.spring.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class SpringDataBithubService implements BithubService {
    @Autowired
    private UserRepository repositoryUser;
    @Autowired
    private BranchRepository repositoryBranch;

    @Autowired
    private FileRepository repositoryFile;

    @Autowired
    private CommitRepository repositoryCommit;

    @Autowired
    private TagRepository repositoryTag;

    @Autowired
    private ReviewRepository repositoryReview;

    @Autowired
    private FileReviewRepository repositoryFileReview;

    @Transactional
    @Override
    public User createUser(String email, String name) {
        User u = new User(email,name);
        return this.repositoryUser.save(u);
    }

    @Transactional
    @Override
    public Optional<User> getUserByEmail(String email) {
        return Optional.empty();
    }

    @Transactional
    @Override
    public Branch createBranch(String name) {
        Branch b = new Branch(name);
        return this.repositoryBranch.save(b);
    }

    @Transactional
    @Override
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {
        Optional<Commit> commit = this.repositoryCommit.findOptionalByHash(commitHash);
        if (commit.isPresent()){
            Tag tag = new Tag(commit.get(),name);
            return this.repositoryTag.save(tag);
        }else {
            throw new BithubException("BithubException");
        }
    }

    @Transactional
    @Override
    public Optional<Commit> getCommitByHash(String commitHash) {
        return this.repositoryCommit.findOptionalByHash(commitHash);
    }

    @Transactional
    @Override
    public File createFile(String content, String name) {
        File file = new File(content,name);
        return this.repositoryFile.save(file);
    }

    @Transactional
    @Override
    public Optional<Tag> getTagByName(String tagName) {
        return this.repositoryTag.findOptionalByName(tagName);
    }

    @Transactional
    @Override
    public Review createReview(Branch branch, User user) {
        Review review = new Review(branch,user);
        return this.repositoryReview.save(review);
    }


    @Transactional
    @Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        if (review.getBranch().equals(file.getCommit().getBranch()))
        {
            FileReview f = new FileReview(review,file,lineNumber,comment);
            return this.repositoryFileReview.save(f);
        }
        else{
            throw new BithubException("BithubException");
        }
    }

    @Transactional
    @Override
    public Optional<Review> getReviewById(Object id) {
        return this.repositoryReview.findById((Long)id);
    }

    @Transactional
    @Override
    public List<Commit> getAllCommitsForUser(Object userId) {
        User u=this.repositoryUser.findById((Long) userId).get();
        return u.getCommits();

    }

    @Transactional
    @Override
    public Map getCommitCountByUser(){
        List<User> users = (List<User>)this.repositoryUser.findAll();
        Map<Long,Long> map = new HashMap<>();
        users.forEach(u->map.put (u.getId(), Long.valueOf(u.getCommits().size())));
        return map;
    }

    @Transactional
    @Override
    public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException {
        Optional<Branch> branch = this.getBranchByName(branchName);
        if (branch.isPresent()){
            Set<User> users = new HashSet<>();
            branch.get().getCommits().forEach(c-> users.add(c.getAuthor()));
            ArrayList<User> list = new ArrayList(users);
            return list;
        }
        else{
            throw new BithubException("BithubException");
        }
    }

    @Transactional
    @Override
    public Optional<Branch> getBranchByName(String branchName) {
        return repositoryBranch.findOptionalByName(branchName);
    }

    @Transactional
    @Override
    public Commit createCommit(String description, String hash, User author, List list, Branch branch) {
        Commit c = new Commit(description,hash,author,list,branch);
        return repositoryCommit.save(c);
    }
}
