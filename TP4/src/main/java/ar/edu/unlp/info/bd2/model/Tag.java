package ar.edu.unlp.info.bd2.model;

import javax.persistence.*;

@Table(name="Tag")
@Entity
public class Tag extends PersistentObject {

    @Column(name = "name", nullable = false, length = 50)
    private  String name;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commit_id")
    private Commit commitHash;



    public Tag(){

    }
    public Tag(Commit commitHash, String name){

        this.commitHash = commitHash;
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Commit getCommitHash() {
        return commitHash;
    }

    public void setCommitHash(Commit commitHash) {
        this.commitHash = commitHash;
    }
}
