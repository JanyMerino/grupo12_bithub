package ar.edu.unlp.info.bd2.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name="Branch")
@Entity
public class Branch extends PersistentObject{

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy="branch")
    private List<Commit> commits= new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy="branch")
    private List<Review> reviews = new ArrayList<>();

    public Branch(){}

    public Branch(String name){
        this.name= name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Commit> getCommits(){
        return commits;
    }

    public void setCommits(List<Commit> commits){
        this.commits=commits;
    }
    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

}
