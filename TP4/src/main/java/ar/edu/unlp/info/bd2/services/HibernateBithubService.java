package ar.edu.unlp.info.bd2.services;

import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.repositories.spring.data.HibernateBithubRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public class HibernateBithubService implements BithubService {

    HibernateBithubRepository repository;
    public HibernateBithubService(HibernateBithubRepository repository) {
        this.repository = repository;
    }


    @Transactional
    @Override
    public User createUser(String email, String name)
    {
        User u = new User();
        u.setName(name);
        u.setEmail(email);
        return this.repository.createUser(u);
    }

    @Transactional
    @Override
    public Optional<User> getUserByEmail(String email)
    {
        return Optional.empty();
    }


    @Transactional
    @Override
    public Branch createBranch(String name) {
        Branch b = new Branch(name);
        return this.repository.createBranch(b);
    }

    @Transactional
    @Override
    public Commit createCommit(String description, String hash, User author, List files, Branch branch) {
        Commit c = new Commit(description, hash, author, files, branch);
        return this.repository.createCommit(c);
    }

    @Transactional
    @Override
    public Tag createTagForCommit(String commitHash, String name) throws BithubException {
        Optional <Commit> c = this.repository.getCommitByHash(commitHash);
        if (c.isPresent()){
            Tag tag = new Tag(c.get(),name);
            return  this.repository.createTagForCommit(tag);
        }
        else {
            throw new BithubException("BithubException");
        }

    }

    @Transactional
    @Override
    public Optional<Commit> getCommitByHash(String commitHash)
    {
        return this.repository.getCommitByHash(commitHash);
    }

    @Transactional
    @Override
    public File createFile(String content, String name)
    {
        File f = new File(content,name);
        return this.repository.createFile(f);
    }

    @Transactional
    @Override
    public Optional<Tag> getTagByName(String tagName)
    {
        return this.repository.getTagByName(tagName);
    }

    @Transactional
    @Override
    public Review createReview(Branch branch, User user)
    {
        Review r = new Review(branch, user);
        return this.repository.createReview(r);
    }

    @Transactional
    @Override
    public FileReview addFileReview(Review review, File file, int lineNumber, String comment) throws BithubException {
        if (review.getBranch().equals(file.getCommit().getBranch()))
        {
            FileReview f = new FileReview(review,file,lineNumber,comment);
            return this.repository.addFileReview(f);
        }
        else{
            throw new BithubException("BithubException");
        }

    }

    @Transactional
    @Override
    public Optional<Review> getReviewById(Object id){ return this.repository.getReviewById((Long)id);}

    @Transactional
    @Override
    public List<Commit> getAllCommitsForUser(Object userId)
    {
        return this.repository.getAllCommitsForUser((Long)userId);
    }

    @Transactional
    @Override
    public Map<Long, Long> getCommitCountByUser()
    {
        return this.repository.getCommitCountByUser();
    }

    @Transactional
    @Override
    public List<User> getUsersThatCommittedInBranch(String branchName) throws BithubException
    {
        Optional<Branch> branch = this.getBranchByName(branchName);
        if (branch.isPresent()){
            return this.repository.getUsersThatCommittedInBranch(branchName);
        }
        else{
            throw new BithubException("BithubException");
        }
    }

    @Transactional
    @Override
    public Optional<Branch> getBranchByName(String branchName)
    {
        return this.repository.getBranchByName(branchName);
    }

}
