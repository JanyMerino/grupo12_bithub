package ar.edu.unlp.info.bd2.repositories.spring.data;

import ar.edu.unlp.info.bd2.model.*;
import ar.edu.unlp.info.bd2.mongo.Association;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Filters.eq;

public class MongoDBBithubRepository {

  @Autowired
  private MongoClient client;
  @Transactional
  public User createUser(User u) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<User> userMongoCollection = db.getCollection("User", User.class);
    userMongoCollection.insertOne(u);
    return u;
  }

  @Transactional
  public Branch createBranch(Branch b) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<Branch> branchMongoCollection = db.getCollection("Branch", Branch.class);
    branchMongoCollection.insertOne(b);
    return b;
  }

  @Transactional
  public Commit createCommit(Commit c) {
    MongoDatabase db = this.client.getDatabase("bd2");
    //insert commit
    db.getCollection("Commit", Commit.class).insertOne(c);


    return c;
  }

  @Transactional
  public Optional<Commit> getCommitByHash(String commitHash) {
    MongoDatabase db = this.client.getDatabase("bd2");
    Optional<Commit> commit = Optional.ofNullable(db.getCollection("Commit", Commit.class).find(eq("hash", commitHash)).first());

    if (commit.isPresent()) {
      Document id_author = db.getCollection("CommitAuthor").aggregate(Arrays.asList(
              Aggregates.match(Filters.eq("source", commit.get().getObjectId())),
              Aggregates.project(Projections.include("destination"))
      )).first();

      User author = db.getCollection("User", User.class).find(eq("_id", id_author.get("destination"))).first();
      commit.get().setAuthor(author);
    }
    return commit;
  }

  public Tag createTagForCommit(Tag tag) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<Tag> userMongoCollection = db.getCollection("Tag", Tag.class);
    userMongoCollection.insertOne(tag);
    return tag;
  }

  public File createFile(File f) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<File> userMongoCollection = db.getCollection("File", File.class);
    userMongoCollection.insertOne(f);
    return f;
  }

  public Optional<Tag> getTagByName(String tagName) {
    MongoDatabase db = this.client.getDatabase("bd2");
    Tag tag = db.getCollection("Tag", Tag.class).find(eq("name", tagName)).first();
    return Optional.ofNullable(tag);
  }

  public Review createReview(Review r) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<Review> reviewMongoCollection = db.getCollection("Review", Review.class);
    reviewMongoCollection.insertOne(r);
    return r;
  }


  public FileReview addFileReview(FileReview f) {
    MongoDatabase db = this.client.getDatabase("bd2");
    MongoCollection<FileReview> fileReviewMongoCollection = db.getCollection("FileReview", FileReview.class);
    fileReviewMongoCollection.insertOne(f);
    return f;
  }

  public Map<Object, Long> getCommitCountByUser() {
    MongoDatabase db = this.client.getDatabase("bd2");
    ArrayList<User> users = db.getCollection("User", User.class).find().into(new ArrayList<User>());
    Map<Object, Long> map = new HashMap<Object, Long>();
    users.forEach(u->map.put (u.getObjectId(), Long.valueOf(u.getCommits().size())));
    return map;
  }

  public List<User> getUsersThatCommittedInBranch(String branchName) {
    return null;
  }


  public Optional<Branch> getBranchByName(String branchName) {
    MongoDatabase db = this.client.getDatabase("bd2");
    Branch b = db.getCollection("Branch", Branch.class).find(eq("name", branchName)).first();
    return Optional.ofNullable(b);
  }

  public Optional<Review> getReviewById(Object id) {
    MongoDatabase db = this.client.getDatabase("bd2");
    Review review = db.getCollection("Review", Review.class).find(eq("_id", id)).first();

    if (Optional.ofNullable(review).isPresent()) {
      Document autor_id = db.getCollection("ReviewAuthor").aggregate(Arrays.asList(
              Aggregates.match(Filters.eq("source", id)),
              Aggregates.project(Projections.include("destination"))
      )).first();

      Document branch_id = db.getCollection("ReviewBranch").aggregate(Arrays.asList(
              Aggregates.match(Filters.eq("source", id)),
              Aggregates.project(Projections.include("destination"))
      )).first();

      Document fileReview_id = db.getCollection("FileReviewReview").aggregate(Arrays.asList(
              Aggregates.match(Filters.eq("destination", id)),
              Aggregates.project(Projections.include("source"))
      )).first();

      Document file_id = db.getCollection("FileReviewFile").aggregate(Arrays.asList(
              Aggregates.match(Filters.eq("source", fileReview_id.get("source"))),
              Aggregates.project(Projections.include("destination"))
      )).first();

      User author = db.getCollection("User", User.class).find(eq("_id", autor_id.get("destination"))).first();
      Branch branch = db.getCollection("Branch", Branch.class).find(eq("_id", branch_id.get("destination"))).first();
      FileReview fileReview = db.getCollection("FileReview", FileReview.class).find(eq("_id", fileReview_id.get("source"))).first();
      File file = db.getCollection("File",File.class).find(eq("_id",file_id.get("destination"))).first();

      review.setBranch(branch);
      review.setAuthor(author);
      fileReview.setReview(review);
      fileReview.setReviewedFile(file);
      review.getReviews().set(0,fileReview);

    }
    return Optional.ofNullable(review);
  }

  public List<Commit> getAllCommitsForUser(Object userId) {
    MongoDatabase db = this.client.getDatabase("bd2");
    User author = db.getCollection("User",User.class).find(eq("_id",userId)).first();
    Document commits = db.getCollection("Author").aggregate(Arrays.asList(
            Aggregates.match(Filters.eq("_id",userId)),
            Aggregates.project(Projections.include("commits"))
    )).first();

    return author.getCommits();
  }

  public void createAsocciation(ObjectId source, ObjectId destino, String nombre) {
    Association association = new Association(source, destino);
    MongoDatabase db = this.client.getDatabase("bd2");
    db.getCollection(nombre, Association.class).insertOne(association);
  }

  public void createAssociationFile(List<File> source, ObjectId destino, String name) {
    this.createAsocciation(source.get(0).getObjectId(), destino, name);
  }


  public void updateBranch(Branch b) {
    MongoDatabase db = this.client.getDatabase("bd2");
    db.getCollection("Branch", Branch.class).replaceOne(Filters.eq("_id", b.getObjectId()), b);
  }

  public void updateAuthor(User a) {
    MongoDatabase db = this.client.getDatabase("bd2");
    db.getCollection("User", User.class).replaceOne(Filters.eq("_id", a.getObjectId()), a);
  }

  public void updateReview(Review r) {
    MongoDatabase db = this.client.getDatabase("bd2");
    db.getCollection("Review", Review.class).replaceOne(Filters.eq("_id", r.getObjectId()), r);
  }

  public void updateFile(File f) {
    MongoDatabase db = this.client.getDatabase("bd2");
    db.getCollection("File", File.class).replaceOne(Filters.eq("_id", f.getObjectId()), f);
  }


  public Review getFindReview(Review r) {
    MongoDatabase db = this.client.getDatabase("bd2");
    Document branch_id = db.getCollection("ReviewBranch").aggregate(Arrays.asList(
            Aggregates.match(Filters.eq("source", r.getObjectId())),
            Aggregates.project(Projections.include("destination"))
    )).first();
    Branch branch = db.getCollection("Branch", Branch.class).find(eq("_id", branch_id.get("destination"))).first();
    r.setBranch(branch);


    return r;
  }

  public ArrayList<Commit> getCommitByBranch(Branch branch) {
    MongoDatabase db = this.client.getDatabase("bd2");
    ArrayList<Commit> listCommits = new ArrayList<Commit>();
    for (Commit commit : branch.getCommits()) {
      Document idAuthor = db.getCollection("CommitAuthor").aggregate(Arrays.asList(
              match(Filters.eq("source", commit.getObjectId())),
              project(Projections.include("destination"))
      )).first();

      User author = db.getCollection("User", User.class).find(eq("_id", idAuthor.get("destination"))).first();

      commit.setAuthor(author);
      listCommits.add(commit);
    }
    return listCommits;
  }
}
