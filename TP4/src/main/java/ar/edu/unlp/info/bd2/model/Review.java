package ar.edu.unlp.info.bd2.model;


import org.bson.codecs.pojo.annotations.BsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name="Review")
@Entity
public class Review extends  PersistentObject{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id")
    @BsonIgnore
    private Branch branch;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @BsonIgnore
    private User author;

    @OneToMany(mappedBy="review")
    private List<FileReview> reviews = new ArrayList<>();

    public Review(){}

    public Review(Branch branch, User user){

        this.branch = branch;
        this.branch.getReviews().add(this);
        this.author = user;
        this.author.getReviews().add(this);

    }


    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User user) {
        this.author = user;
    }

    public void setReviews(List<FileReview> reviews) {
        this.reviews = reviews;
    }
    public List<FileReview> getReviews(){
        return this.reviews;
    }
}
