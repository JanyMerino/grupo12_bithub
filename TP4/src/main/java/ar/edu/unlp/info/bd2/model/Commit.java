package ar.edu.unlp.info.bd2.model;

import org.bson.codecs.pojo.annotations.BsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Table(name="Commit")
@Entity
public class Commit extends PersistentObject {

    @Column(name = "description", nullable = false, length = 50)
    private String description;

    @Column(name = "hash", nullable = false, length = 50)
    private String hash;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @BsonIgnore
    private User author;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "commit")
    private List<File> files = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "branch_id")
    @BsonIgnore
    private Branch branch;

    public Commit(){}

    public Commit(String description, String hash, User author, List<File> files, Branch branch){
        this.description= description;
        this.hash= hash;
        this.author= author;
        this.author.getCommits().add(this);
        this.branch= branch;
        this.branch.getCommits().add(this);
        this.files = files;
        this.files.forEach(f -> f.setCommit(this));

    }



    public String getMessage() {
        return description;
    }

    public void setMessage(String description) {
        this.description = description;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public Branch getBranch() {
        return branch;
    }


}



