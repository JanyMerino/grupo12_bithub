package ar.edu.unlp.info.bd2.repositories.spring.data;

import ar.edu.unlp.info.bd2.model.Commit;
import ar.edu.unlp.info.bd2.model.File;
import ar.edu.unlp.info.bd2.model.Tag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface TagRepository extends CrudRepository<Tag, Long> {
    Optional<Tag> findOptionalByName(String name);
}
